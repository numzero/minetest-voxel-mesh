// Voxel to mesh converter
// Copyright (C) 2019 Lobachevskiy Vitaliy <numzer0@yandex.ru>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#include <cassert>
#include <cstring>
#include <algorithm>
#include <chrono>
#include <deque>
#include <list>
#include <map>
#include <memory>
#include <random>
#include <set>
#include <GLFW/glfw3.h>

static constexpr int mesh_size = 16;

static std::mt19937_64 prng(std::chrono::system_clock::now().time_since_epoch().count());

bool pixels[mesh_size][mesh_size];

void fill_pixels() {
	std::bernoulli_distribution d(0.5);
	for (int j = 0; j < mesh_size; j++)
		for (int i = 0; i < mesh_size; i++)
			pixels[j][i] = d(prng);
}

void draw_pixels() {
	glBegin(GL_QUADS);
	for (int j = 0; j < mesh_size; j++)
		for (int i = 0; i < mesh_size; i++)
			if (pixels[j][i]) {
				glVertex2f(i, j);
				glVertex2f(i + 1, j);
				glVertex2f(i + 1, j + 1);
				glVertex2f(i, j + 1);
			}
	glEnd();
}

float sqr(float x) {
	return x * x;
}

void trangulate() {
	struct Coords {
		int x, y;
	};
	struct Vertex {
		bool nn, pn, np, pp;
		enum Type {
			Empty,
			OuterCorner,
			Edge,
			InnerCorner,
			Inner,
			Join,
		} type;
	};

	static constexpr float colors[][3] = {
		{0.0f, 0.0f, 1.0f},
		{1.0f, 1.0f, 0.0f},
		{0.0f, 1.0f, 0.0f},
		{1.0f, 0.0f, 0.0f},
		{0.0f, 0.3f, 0.0f},
		{1.0f, 0.0f, 1.0f},
	};
	auto colorize = [] (Vertex::Type type, float alpha = 1.0f) {
		assert(type < 6);
		float const *color = colors[type];
		glColor4f(color[0], color[1], color[2], alpha);
	};

	bool pixels2[mesh_size + 2][mesh_size + 2];
	Vertex vertices[mesh_size + 1][mesh_size + 1];

	std::memset(pixels2, 0, sizeof(pixels2));

	for (int j = 0; j < mesh_size; j++)
		for (int i = 0; i < mesh_size; i++)
			pixels2[j + 1][i + 1] = pixels[j][i];

	for (int i = 0; i < mesh_size; i++)
		pixels2[mesh_size + 1][i + 1] = !pixels[mesh_size - 1][i];

	for (int j = 0; j <= mesh_size; j++)
		for (int i = 0; i <= mesh_size; i++) {
			Vertex &v = vertices[j][i];
			v.nn = pixels2[j][i];
			v.pn = pixels2[j][i + 1];
			v.np = pixels2[j + 1][i];
			v.pp = pixels2[j + 1][i + 1];
			int n = v.nn + v.pn + v.np + v.pp;
			if (n == 2)
				v.type = (v.nn == v.pn) || (v.nn == v.np) ? Vertex::Edge : Vertex::Join;
			else
				v.type = (Vertex::Type)n;
		}

	struct XSort {
		bool operator() (Coords const &a, Coords const &b) const noexcept {
			return a.x < b.x;
		}
	};
	struct YSort {
		bool operator() (Coords const &a, Coords const &b) const noexcept {
			return a.y < b.y;
		}
	};
	struct Line {
		int x1, y1;
		int x2, y2;
		bool type;
	};
	using PLine = std::shared_ptr<Line>;
	struct X2Sort {
		bool operator() (PLine const &a, PLine const &b) const noexcept {
			return a->x2 < b->x2;
		}
	};
	struct Triangle {
		int x1, y1;
		int x2, y2;
		int x3, y3;
		bool type;
	};
	std::deque<Triangle> tris;
	std::array<PLine, mesh_size> edges;
	edges.fill(std::make_shared<Line>(Line{0, 0, mesh_size, 0, 0}));
	for (int y = 0; y <= mesh_size; y++) {
		for (int x = 0; x <= mesh_size; x++) {
			Vertex &v = vertices[y][x];
			if (v.type == Vertex::Empty || v.type == Vertex::Inner || v.type == Vertex::Edge)
				continue;
			if (x > 0 && x < mesh_size && edges[x - 1] == edges[x]) { // single edge, splitting
				auto l = edges[x - 1];
				tris.push_back({l->x1, l->y1, l->x2, l->y2, x, y, l->type});
				auto a = std::make_shared<Line>(Line{l->x1, l->y1, x, y, l->type});
				auto b = std::make_shared<Line>(Line{x, y, l->x2, l->y2, l->type});
				for (int k = l->x1; k < x; k++)
					edges[k] = a;
				for (int k = x; k < l->x2; k++)
					edges[k] = b;
				continue;
			}
			if (x > 0) {
				auto l = edges[x - 1];
				tris.push_back({l->x1, l->y1, l->x2, l->y2, x, y, l->type});
				l->y2 = y;
			}
			if (x < mesh_size) {
				auto l = edges[x];
				tris.push_back({l->x1, l->y1, l->x2, l->y2, x, y, l->type});
				l->y1 = y;
			}
		}
		std::set<PLine, X2Sort> lines;
		for (int x = 0; x <= mesh_size; x++) {
			Vertex &v = vertices[y][x];
			if (v.type == Vertex::Empty || v.type == Vertex::Inner)
				continue;
			if (v.type == Vertex::Edge) {
				if (v.nn == v.np)
					continue; // vertical edge
				// horizontal edge
				assert(x > 0);
				lines.insert(edges[x - 1]);
				continue;
			}
			if (lines.empty())
				continue;
			auto last = *lines.rbegin();
			int x1 = (**lines.begin()).x1;
			int x2 = x;
			auto l = std::make_shared<Line>(Line{x1, y, x2, y});
			for (int i = x1; i < x2; i++)
				edges[i] = l;
			std::set<Coords, XSort> points;
			points.insert({x1, y});
			points.insert({x2, y});
			for (auto l: lines)
				points.insert({l->x2, l->y2});
			while (points.size() > 2) {
				auto pb = std::min_element(points.begin(), points.end(), YSort());
				assert(pb != points.begin());
				assert(pb != points.rbegin().base());
				auto pa = pb, pc = pb;
				--pa, ++pc;
				tris.push_back({pa->x, pa->y, pb->x, pb->y, pc->x, pc->y, last->type});
				points.erase(pb);
			}
			lines.clear();
		}
		for (int i = 0; i < mesh_size; i++) {
			auto e = edges[i];
			if (e->y1 == e->y2)
				e->type = pixels[y][i];
		}
	}
	glBegin(GL_TRIANGLES);
	for (auto tri: tris) {
		if (tri.type)
			glColor4f(1.0f, 1.0f, 0.0f, 0.3f);
		else
			glColor4f(0.0f, 0.0f, 1.0f, 0.3f);
		glVertex2iv(&tri.x1);
		glVertex2iv(&tri.x2);
		glVertex2iv(&tri.x3);
	}
	glEnd();
	glLineWidth(1.0f);
	for (auto tri: tris) {
		if (tri.type)
			glColor4f(1.0f, 1.0f, 0.0f, 0.5f);
		else
			glColor4f(0.0f, 0.0f, 1.0f, 0.5f);
		glBegin(GL_LINE_LOOP);
		glVertex2iv(&tri.x1);
		glVertex2iv(&tri.x2);
		glVertex2iv(&tri.x3);
		glEnd();
	}
}

GLFWwindow *window;

int main() {
	if (!glfwInit())
		return -1;

	glfwWindowHint(GLFW_DOUBLEBUFFER, GLFW_TRUE);
	glfwWindowHint(GLFW_SAMPLES, 8);
	window = glfwCreateWindow(800, 600, "Experiment: Voxel Mesh", NULL, NULL);
	if (!window) {
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glClearColor(0.0f, 0.0f, 0.5f, 1.0f);
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glPointSize(5.0);

	fill_pixels();

	while (!glfwWindowShouldClose(window)) {
		int w, h;
		glfwGetWindowSize(window, &w, &h);
		glViewport(0, 0, w, h);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-0.125, 1.125, -0.125, 1.125, -0.125, 1.125);
		glMatrixMode(GL_MODELVIEW);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glLoadIdentity();
		glColor4f(0.0f, 0.5f, 0.0f, 1.0f);
		double s = 1.0 / mesh_size;
		glScalef(s, s, s);
		draw_pixels();
		trangulate();

		glFlush();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
